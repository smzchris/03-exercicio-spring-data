package com.itau.funcionarios.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.funcionarios.models.Localizacao;
import com.itau.funcionarios.repositories.LocalizacaoRepository;

@Service
public class LocalizacaoService {
	@Autowired
	LocalizacaoRepository localizacaoRepository;
	
	public Iterable<Localizacao> buscarTodos(){
		return localizacaoRepository.findAll();
	}
}
